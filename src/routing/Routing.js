/* eslint-disable prettier/prettier */
/* eslint-disable no-unused-vars */
import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import HomeScreen from '../pages/Home/HomeScreen';
import Wizard1 from '../pages/Wizard/Wizard1';
import Wizard2 from '../pages/Wizard/Wizard2';
import Wizard3 from '../pages/Wizard/Wizard3';

const Stack = createNativeStackNavigator();

export default function Routing({Navigator, route}) {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>

        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="wizard1" component={Wizard1} />
        <Stack.Screen name="wizard2" component={Wizard2} />
        <Stack.Screen name="wizard3rd" component={Wizard3} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

// export default Routing;
