/* eslint-disable prettier/prettier */
/* eslint-disable comma-dangle */
import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';

const HomeScreen = ({navigation}) => {
  return (
    // eslint-disable-next-line react-native/no-inline-styles
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <TouchableOpacity
        // eslint-disable-next-line react-native/no-inline-styles
        onPress={() => navigation.navigate('wizard1')}

        style={{
          backgroundColor: 'brown',
          paddingHorizontal: 30,
          paddingVertical: 10,
          borderRadius: 20,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Text style={{color: 'white', fontSize: 18, fontWeight: '600'}}>
          Start
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default HomeScreen;
