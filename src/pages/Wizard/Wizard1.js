/* eslint-disable prettier/prettier */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */

import React, {useEffect, useState} from 'react';
import RNPickerSelect from 'react-native-picker-select';
import {
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  Touchable,
  TouchableOpacity,
  View,
} from 'react-native';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SelectDropdown from 'react-native-select-dropdown';
import {SelectList} from 'react-native-dropdown-select-list';
import {Dropdown} from 'react-native-element-dropdown';
// import {getProvinsi, getKabupaten} from '../../utils/data';

const Wizard1 = ({navigation}) => {
  const [selectedProvince, setSelectedProvince] = useState('');
  const [provinsi, setProvinsi] = useState([]);
  const [selectedCity, setSelectedCity] = useState('');
  const [text, setText] = useState(null);
  const [kabupaten, setKabupaten] = useState([]);
  const [selectedStreet, setSelectedStreet] = useState('');
  const [streets, setStreets] = useState([]);
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [bio, setBio] = useState('');

  const getProvinsi = async () => {
    const url =
      'https://api.binderbyte.com/wilayah/provinsi?api_key=9752656a5bada5bbea6e9ac022f3d5f9ca9785f5f651eb1148ecb482b01c3b89';
    try {
      const response = await fetch(url);
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      const prov = await response.json();
      setProvinsi(prov.value);
      return prov.value;
    } catch (error) {
      console.error('Error fetching data:', error);
      return [];
    }
  };

  const getKabupaten = async provinceId => {
    const url = `https://api.binderbyte.com/wilayah/kabupaten?api_key=9752656a5bada5bbea6e9ac022f3d5f9ca9785f5f651eb1148ecb482b01c3b89&id_provinsi=${provinceId}`;
    try {
      const response = await fetch(url);
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      const data = await response.json();
      setKabupaten(data.value);
    } catch (error) {
      console.error('Error fetching cities:', error);
    }
  };

  const getStreets = async cityId => {
    const url = `https://api.binderbyte.com/wilayah/kecamatan?api_key=9752656a5bada5bbea6e9ac022f3d5f9ca9785f5f651eb1148ecb482b01c3b89&id_kabupaten=${cityId}`;
    try {
      const response = await fetch(url);
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      const data = await response.json();
      setStreets(data.value);
    } catch (error) {
      console.error('Error fetching streets:', error);
    }
  };

  const handleProvSelect = provinceId => {
    console.log(provinceId);
    setSelectedProvince(provinceId);
    setSelectedCity('');
    getKabupaten(provinceId);
  };

  const handleCitySelect = async cityId => {
    setSelectedCity(cityId);
    setSelectedStreet('');
    getStreets(cityId);
  };

  const handleStreetSelect = streetId => {
    console.log(streetId);
    setSelectedStreet(streetId);
  };

  useEffect(() => {
    getProvinsi();
  }, []);

  const dataProvince = provinsi.map(item => {
    return {key: item.id, value: item.name};
  });

  // console.log(dataProvince);

  const dataCity = kabupaten.map(item => {
    return {key: item.id, value: item.name};
  });

  const dataStreet = streets.map(item => {
    return {key: item.id, value: item.name};
  });

  // console.log(selectedProvince);
  // console.log(selectedCity);
  // console.log(selectedStreet);

  return (
    <ScrollView>
      <View
        style={{
          paddingHorizontal: 15,
          paddingVertical: 10,
          backgroundColor: '#fff',
        }}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <View
            style={{
              borderLeftWidth: 10,
              backgroundColor: '#fff',
              height: 70,
              borderLeftColor: '#1F2081',
              borderTopLeftRadius: 5,
              borderBottomLeftRadius: 10,
              flexDirection: 'row',
              alignItems: 'center',
              shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: 0.7,
              shadowRadius: 3.84,
              elevation: 5,
              flexBasis: '60%',
              marginRight: 30,
            }}>
            <View
              style={{
                backgroundColor: '#EEF0FA',
                marginRight: 10,
                height: 70,
                flexBasis: '30%',
                alignContent: 'center',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <SimpleLineIcons name="notebook" size={40} color={'#1F2081'} />
            </View>

            <Text>Formulir Pelanggan</Text>
          </View>
          <View
            style={{
              borderLeftWidth: 10,
              backgroundColor: '#fff',
              height: 70,
              borderLeftColor: '#6EBB3F',
              borderTopLeftRadius: 5,
              borderBottomLeftRadius: 10,
              flexDirection: 'row',
              alignItems: 'center',
              shadowColor: '#000',
              paddingRight: 50,
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: 0.7,
              shadowRadius: 3.84,
              elevation: 5,
            }}>
            <View
              style={{
                backgroundColor: '#E0F4D7',
                marginRight: 10,
                height: 70,
                flexBasis: '30%',
                alignContent: 'center',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <MaterialCommunityIcons
                name="card-account-details-star-outline"
                size={40}
                color={'#6EBB3F'}
              />
            </View>
          </View>
        </View>
        <View style={{marginTop: 20}}>
          <View
            style={{
              flex: 1,
              backgroundColor: '#EEF0FA',
              // paddingHorizontal: 30,
              paddingVertical: 5,
              alignItems: 'center',
              justifyContent: 'center',
              // marginHorizontal: 10,
              marginBottom: 15,
            }}>
            <Text style={{fontSize: 16, fontWeight: '700', color: '#1F2081'}}>
              Registrasi Pelangan Baru
            </Text>
          </View>
          <View
            style={{
              borderWidth: 0.2,
              paddingHorizontal: 20,
              paddingVertical: 20,
              marginBottom: 20,
              borderRadius: 5,
              justifyContent: 'space-between',
            }}>
            <View style={{justifyContent: 'space-between'}}>
              <Text style={styles.text}>First Name :</Text>
              <TextInput
                placeholder="Enter First Name"
                style={styles.textInput}
                value={firstName}
                onChangeText={setFirstName}
              />
              <Text style={styles.text}>Last Name :</Text>
              <TextInput
                placeholder="Enter Last Name"
                style={styles.textInput}
                value={lastName}
                onChangeText={setLastName}
              />
              <Text style={styles.text}>Bio :</Text>
              <TextInput
                style={styles.textArea}
                multiline={true}
                numberOfLines={4}
                onChangeText={setBio}
                value={bio}
                placeholder="Enter Bio"
              />
              <Text style={styles.text}>Address :</Text>
              <SelectList
                boxStyles={{marginBottom: 10}}
                setSelected={handleProvSelect}
                data={dataProvince}
                save="name"
                placeholder="Select Province"
              />
              {selectedProvince && (
                <SelectList
                  boxStyles={{marginBottom: 10}}
                  setSelected={handleCitySelect}
                  data={dataCity}
                  save="name"
                  placeholder="Select City"
                />
              )}
              {selectedCity && (
                <SelectList
                  data={dataStreet}
                  placeholder="Select Street"
                  setSelected={handleStreetSelect}
                />
              )}
            </View>
          </View>

          <View
            style={{
              flex: 1,
              backgroundColor: 'white',
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 20,
              paddingHorizontal: 5,
            }}>
            <TouchableOpacity
              style={{
                backgroundColor: 'blue',
                paddingHorizontal: 20,
                justifyContent: 'center',
                alignItems: 'center',
                height: 50,
                flexBasis: '45%',
                borderRadius: 10,
              }}>
              <Text style={{color: 'white', fontSize: 16, fontWeight: '600'}}>
                Back
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() =>
                navigation.navigate('wizard2', {
                  firstName: firstName,
                  lastName: lastName,
                  bio: bio,
                  selectedProvince: selectedProvince,
                  selectedCity: selectedCity,
                  selectedStreet: selectedStreet,
                })
              }
              style={{
                backgroundColor: 'green',
                paddingHorizontal: 20,
                justifyContent: 'center',
                alignItems: 'center',
                height: 50,
                flexBasis: '45%',
                borderRadius: 10,
              }}>
              <Text style={{color: 'white', fontSize: 16, fontWeight: '600'}}>
                Next
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default Wizard1;

const styles = StyleSheet.create({
  text: {
    color: '#1F2081',
    fontSize: 14,
    marginVertical: 8,
    fontWeight: '700',
  },
  textArea: {
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 4,
    padding: 8,
    fontSize: 16,
    minHeight: 100,
  },
  textInput: {
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 4,
    fontSize: 16,
    height: 40,
  },
  dropdown: {
    height: 50,
    borderColor: 'gray',
    borderWidth: 0.5,
    borderRadius: 8,
    paddingHorizontal: 8,
  },
  selectedTextStyle: {
    fontSize: 16,
  },
});
