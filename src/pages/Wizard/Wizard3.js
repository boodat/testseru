/* eslint-disable prettier/prettier */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */

import React, {useEffect, useState} from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';

const Wizard3 = ({navigation}) => {
  return (
    <ScrollView>
      <View style={{backgroundColor: '#fff', paddingHorizontal: 20}}>
        <View
          style={{
            flex: 1,
            backgroundColor: '#EEF0FA',
            // paddingHorizontal: 30,
            paddingVertical: 5,
            alignItems: 'center',
            justifyContent: 'center',
            marginHorizontal: 20,
            marginTop: 15,
            marginBottom: 15,
          }}>
          <Text style={{fontSize: 16, fontWeight: '700', color: '#1F2081'}}>
            Registrasi Pelangan Baru
          </Text>
        </View>

        <View
          style={{
            borderWidth: 0.2,
            paddingHorizontal: 20,
            paddingVertical: 20,
            borderRadius: 5,
            flex: 1,
            // flexDirection: 'row',
          }}>
          <View style={{flexDirection: 'row', flex:1,}}>
            <Text style={[styles.text,{flexBasis: '45%'}]}>Full Name</Text>
            <Text  style={[styles.text,{flexBasis: '5%'}]}>:</Text>
            <Text style={[styles.text,{flexBasis: '45%'}]}>Bastian Haloho</Text>
          </View>
          <View style={{flexDirection: 'row', flex:1,}}>
            <Text style={[styles.text,{flexBasis: '45%'}]}>Bio</Text>
            <Text  style={[styles.text,{flexBasis: '5%'}]}>:</Text>
            <Text style={[styles.text,{flexBasis: '45%', minheight:20}]}>Fullstack Developer</Text>
          </View>
          <View style={{flexDirection: 'row', flex:1,}}>
            <Text style={[styles.text,{flexBasis: '45%'}]}>Address</Text>
            <Text  style={[styles.text,{flexBasis: '5%'}]}>:</Text>
            <Text style={[styles.text,{flexBasis: '45%'}]}>SUMATERA UTARA, kabupaten Simalungun, Silimakuta</Text>
          </View>
        </View>
        <View style={{marginTop: 20, padding: 20}}>
          <TouchableOpacity
          onPress={() => navigation.navigate('Home')}
            style={{
              backgroundColor: 'green',
              height: 35,
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: 10
            }}>
            <Text style={{color: '#fff', fontWeight: '600', fontSize: 16}}>Home</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};

export default Wizard3;

const styles = StyleSheet.create({
  text: {
    color: '#1F2081',
    fontSize: 14,
    marginVertical: 8,
    fontWeight: '700',
  },
  textArea: {
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 4,
    padding: 8,
    fontSize: 16,
    minHeight: 100,
  },
  textInput: {
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 4,
    fontSize: 16,
    height: 40,
  },
  dropdown: {
    height: 50,
    borderColor: 'gray',
    borderWidth: 0.5,
    borderRadius: 8,
    paddingHorizontal: 8,
  },
  selectedTextStyle: {
    fontSize: 16,
  },
});
