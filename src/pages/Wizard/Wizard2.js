/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {
  View,
  Image,
  Button,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';
import * as ImagePicker from 'react-native-image-picker';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';

const Wizard2 = ({navigation}) => {
  const [selectedImage, setSelectedImage] = useState(null);

  const openCamera = () => {
    const options = {
      storageOptions: {
        path: 'images',
        mediaType: 'photo',
      },
      includeBase64: true,
    };

    launchCamera(options, response => {
      console.log('Response = ', response);
      if (response.didCancel) {
        console.log('user cancel camera');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('user tapped the custome button: ', response.customButton);
      } else {
        const source = {uri: 'data:image/jpeg;base64' + response.base64};
      }
    });
  };

  return (
    <View style={styles.container}>
      <View style={styles.imageContainer}>
        {selectedImage && <Image source={selectedImage} style={styles.image} />}
      </View>
      <TouchableOpacity
        style={{
          backgroundColor: 'teal',
          paddingHorizontal: 20,
          height: 40,
          justifyContent: 'center',
          alignItems: 'center',
          borderRadius: 10
        }}
        onPress={() => {
          openCamera();
        }}>
        <Text style={{color: '#fff', fontSize: 14, fontWeight: '600'}}>Upload Image</Text>
      </TouchableOpacity>

      <View
        style={{
          flex: 1,
          backgroundColor: 'white',
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginTop: 20,
          paddingHorizontal: 5,
        }}>
        <TouchableOpacity
          style={{
            backgroundColor: 'blue',
            paddingHorizontal: 20,
            justifyContent: 'center',
            alignItems: 'center',
            height: 50,
            flexBasis: '45%',
            borderRadius: 10,
          }}>
          <Text style={{color: 'white', fontSize: 16, fontWeight: '600'}}>
            Back
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate('wizard3rd')}
          style={{
            backgroundColor: 'green',
            paddingHorizontal: 20,
            justifyContent: 'center',
            alignItems: 'center',
            height: 50,
            flexBasis: '45%',
            borderRadius: 10,
          }}>
          <Text style={{color: 'white', fontSize: 16, fontWeight: '600'}}>
            Next
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageContainer: {
    width: '80%',
    height: 200,
    marginBottom: 20,
    borderWidth: 1,
    borderColor: '#ccc',
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: '100%',
    height: '100%',
  },
});

export default Wizard2;
