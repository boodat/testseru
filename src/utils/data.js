/* eslint-disable prettier/prettier */
const getProvinsi = async () => {
  const url =
    'https://api.binderbyte.com/wilayah/provinsi?api_key=9752656a5bada5bbea6e9ac022f3d5f9ca9785f5f651eb1148ecb482b01c3b89';

  try {
    const response = await fetch(url);
    if (!response.ok) {
      throw new Error('Network response was not ok');
    }
    const prov = await response.json();
    const provinsiData = prov.value;
    return provinsiData;
  } catch (error) {
    console.error('Error fetching data:', error);
    return [];
  }
};



export { getProvinsi, };